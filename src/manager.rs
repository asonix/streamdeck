use crate::{
    deck::Deck,
    message::{DeckConfig, InputMessage, ManagerMessage},
    port::Port,
};
use serialport::{SerialPortType, UsbPortInfo};
use std::{
    collections::{HashMap, HashSet},
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};
use tokio::{
    sync::mpsc::{Receiver, Sender},
    task::JoinHandle,
    time::Duration,
};

struct Manager {
    input_tx: Sender<InputMessage>,
    tx: Sender<ManagerMessage>,
    decks: HashMap<String, DeckConfig>,
    ports: HashMap<String, JoinHandle<()>>,
    opening: HashMap<String, JoinHandle<()>>,
    finding: Option<JoinHandle<()>>,
    discovery: Option<JoinHandle<()>>,
}

impl Manager {
    fn new(input_tx: Sender<InputMessage>, tx: Sender<ManagerMessage>) -> Self {
        let discovery = tokio::spawn(tick_task(tx.clone()));
        Manager {
            input_tx,
            tx,
            decks: HashMap::default(),
            ports: HashMap::default(),
            opening: HashMap::default(),
            finding: None,
            discovery: Some(discovery),
        }
    }

    fn turn<'a>(&'a mut self, msg: ManagerMessage) -> Turn<'a> {
        Turn(self, Some(msg))
    }

    fn do_turn(&mut self, msg: ManagerMessage, cx: &mut Context<'_>) {
        log::debug!("msg: {:?}", msg);
        log::debug!("state: {:?}", self);
        match msg {
            ManagerMessage::EnableDiscovery => {
                if let Some(mut handle) = self.discovery.take() {
                    if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                        self.discovery = Some(handle);
                        return;
                    }
                }

                let handle = tokio::spawn(tick_task(self.tx.clone()));
                self.discovery = Some(handle);
            }
            ManagerMessage::DisableDiscovery => {
                if let Some(handle) = self.discovery.take() {
                    handle.abort();
                }
            }
            ManagerMessage::Tick => {
                if let Some(mut handle) = self.finding.take() {
                    if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                        self.finding = Some(handle);
                        return;
                    }
                }

                let handle = tokio::spawn(find_task(self.tx.clone()));
                self.finding = Some(handle);
            }
            ManagerMessage::List(sender) => {
                let _ = sender.send(self.decks.values().cloned().collect());
            }
            ManagerMessage::Found(mut found_decks) => {
                let known = self.ports.keys().cloned().collect::<HashSet<_>>();
                let found = found_decks.keys().cloned().collect::<HashSet<_>>();

                for port_name in found.difference(&known) {
                    if let Some(mut handle) = self.opening.remove(port_name) {
                        if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                            self.opening.insert(port_name.clone(), handle);
                            continue;
                        }
                    }

                    if let Some(config) = found_decks.remove(port_name) {
                        log::info!("New deck: {}", config.port_name);
                        let handle = tokio::spawn(open_task(config, self.tx.clone()));
                        self.opening.insert(port_name.clone(), handle);
                    }
                }

                for port_name in known.difference(&found) {
                    log::info!("Removed deck: {}", port_name);
                    if let Some(handle) = self.ports.remove(port_name) {
                        handle.abort();
                    }
                    self.decks.remove(port_name);
                }
            }
            ManagerMessage::Opened(deck, port) => {
                let handle = tokio::spawn(
                    Deck {
                        manager: self.tx.clone(),
                        input: self.input_tx.clone(),
                        port,
                        config: deck.clone(),
                    }
                    .run(),
                );
                self.ports.insert(deck.port_name.clone(), handle);
                self.decks.insert(deck.port_name.clone(), deck);
            }
            ManagerMessage::Closed(port_name) => {
                if let Some(handle) = self.ports.remove(&port_name) {
                    handle.abort();
                }
                self.decks.remove(&port_name);
            }
        }
    }
}

pub(crate) async fn task(
    input_tx: Sender<InputMessage>,
    tx: Sender<ManagerMessage>,
    mut rx: Receiver<ManagerMessage>,
) {
    let mut state = Manager::new(input_tx, tx);

    while let Some(msg) = rx.recv().await {
        state.turn(msg).await;
    }
}

struct Turn<'a>(&'a mut Manager, Option<ManagerMessage>);

impl<'a> Future for Turn<'a> {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let msg = self.1.take().unwrap();
        self.0.do_turn(msg, cx);
        Poll::Ready(())
    }
}

async fn tick_task(tx: Sender<ManagerMessage>) {
    let mut interval = tokio::time::interval(Duration::from_secs(5));

    loop {
        let _ = tx.send(ManagerMessage::Tick).await;
        interval.tick().await;
    }
}

async fn find_task(tx: Sender<ManagerMessage>) {
    match find_streamdecks().await {
        Ok(streamdecks) => {
            let _ = tx.send(ManagerMessage::Found(streamdecks)).await;
        }
        Err(e) => log::error!("Error finding streamdecks: {}", e),
    }
}

async fn open_task(deck: DeckConfig, tx: Sender<ManagerMessage>) {
    match Port::open(deck.port_name.clone()).await {
        Ok(port) => {
            let _ = tx.send(ManagerMessage::Opened(deck, port)).await;
        }
        Err(e) => log::error!("Error opening streamdeck {}: {}", deck.port_name, e),
    }
}

async fn find_streamdecks() -> Result<HashMap<String, DeckConfig>, anyhow::Error> {
    tokio::task::spawn_blocking(|| {
        Ok(serialport::available_ports()?
            .into_iter()
            .filter_map(|port| {
                if let SerialPortType::UsbPort(info) = port.port_type {
                    if is_valid_port(&info) {
                        return Some((
                            port.port_name.clone(),
                            DeckConfig {
                                port_name: port.port_name,
                                product_name: info.product?,
                                serial_number: info.serial_number?,
                            },
                        ));
                    }
                }

                None
            })
            .collect())
    })
    .await?
}

fn is_valid_port(_: &UsbPortInfo) -> bool {
    // TODO: Check for BasedOtt pid/vid
    true
}

impl std::fmt::Debug for Manager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ports = self.ports.keys().map(|s| s.as_str()).collect::<Vec<_>>();

        f.debug_struct("Manager").field("ports", &ports).finish()
    }
}
