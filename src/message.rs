use crate::port::Port;
use std::collections::HashMap;
use tokio::sync::oneshot::Sender;

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
#[serde(tag = "type")]
pub(crate) enum Command {
    SwitchScene { name: String },
}

#[derive(Debug)]
pub(crate) enum Query {
    GetScenes(Sender<Vec<String>>),
}

#[derive(Debug)]
pub(crate) enum State {
    Disconnected,
    Unauthenticated,
    Connected,
}

#[derive(Debug)]
pub(crate) enum ObsMessage {
    Query(Query),
    Command(Command),
    Connect(String, u16),
    Authenticate(String),
    State(Sender<State>),
    Disconnect,
}

pub(crate) enum InputMessage {
    Press(String, u8),
    ReadInput(Sender<(String, u8)>),
}

#[derive(Clone, Debug)]
pub(crate) struct DeckConfig {
    pub(crate) port_name: String,
    pub(crate) product_name: String,
    pub(crate) serial_number: String,
}

#[derive(Debug)]
pub(crate) enum ManagerMessage {
    EnableDiscovery,
    DisableDiscovery,
    Tick,
    List(Sender<Vec<DeckConfig>>),
    Found(HashMap<String, DeckConfig>),
    Opened(DeckConfig, Port),
    Closed(String),
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            State::Disconnected => write!(f, "Disconnected"),
            State::Unauthenticated => write!(f, "Unauthenticated"),
            State::Connected => write!(f, "Connected"),
        }
    }
}
