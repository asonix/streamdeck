use crate::{
    message::{InputMessage, ObsMessage},
    store::Store,
};
use tokio::sync::{
    mpsc::{Receiver, Sender},
    oneshot,
};

pub(crate) async fn task(store: Store, mut rx: Receiver<InputMessage>, obs_tx: Sender<ObsMessage>) {
    let mut sender_opt: Option<oneshot::Sender<(String, u8)>> = None;

    while let Some(msg) = rx.recv().await {
        match msg {
            InputMessage::Press(serial_number, key) => {
                log::debug!("{}: {}", serial_number, key);

                if let Some(sender) = sender_opt.take() {
                    let _ = sender.send((serial_number, key));
                } else {
                    if let Ok(Some(command)) = store.pressed(&serial_number, key).await {
                        let _ = obs_tx.send(ObsMessage::Command(command)).await;
                    }
                }
            }
            InputMessage::ReadInput(sender) => {
                sender_opt = Some(sender);
            }
        }
    }
}
