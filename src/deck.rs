use crate::{
    message::{DeckConfig, InputMessage, ManagerMessage},
    port::Port,
};
use std::{future::Future, pin::Pin};
use tokio::{sync::mpsc::Sender, time::Duration};

#[derive(Debug)]
pub(crate) struct Deck {
    pub(crate) manager: Sender<ManagerMessage>,
    pub(crate) input: Sender<InputMessage>,
    pub(crate) port: Port,
    pub(crate) config: DeckConfig,
}

impl Deck {
    pub(crate) async fn run(self) {
        let res = self.read_task();
        if let Ok(fut) = res {
            let _ = tokio::spawn(fut).await;
        }

        log::info!("{} disconnected", self.config.product_name);

        while self
            .manager
            .send(ManagerMessage::Closed(self.config.port_name.clone()))
            .await
            .is_err()
        {
            if self.manager.is_closed() {
                return;
            }

            tokio::time::sleep(Duration::from_secs(5)).await;
        }
    }

    fn read_task(
        &self,
    ) -> Result<Pin<Box<dyn Future<Output = ()> + Send + 'static>>, anyhow::Error> {
        let mut read_port = self.port.try_clone()?.bytes();
        let input = self.input.clone();
        let serial_number = self.config.serial_number.clone();

        Ok(Box::pin(async move {
            loop {
                log::trace!("read loop");
                match read_port.read_byte().await {
                    Ok(byte) => {
                        let _ = input
                            .send(InputMessage::Press(serial_number.clone(), byte))
                            .await;
                    }
                    Err(e) => {
                        log::trace!("Error reading: {}", e);
                        if let Ok(e) = e.downcast::<std::io::Error>() {
                            if matches!(e.kind(), std::io::ErrorKind::BrokenPipe) {
                                break;
                            }
                        }
                    }
                }
            }
        }))
    }
}
