use directories::ProjectDirs;
use std::sync::Arc;
use tokio::sync::Notify;

#[cfg(feature = "ipc-dbus")]
mod dbus;
mod deck;
mod input;
mod manager;
mod message;
mod obs;
mod port;
mod store;

use store::Store;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }
    env_logger::init();

    let (manager_tx, manager_rx) = tokio::sync::mpsc::channel(16);
    let (input_tx, input_rx) = tokio::sync::mpsc::channel(16);
    let (obs_tx, obs_rx) = tokio::sync::mpsc::channel(16);

    let project_dirs = ProjectDirs::from("dog", "asonix", "Streamdeck")
        .ok_or_else(|| anyhow::anyhow!("No home directory found"))?;
    let mut sled_dir = project_dirs.data_local_dir().to_owned();
    sled_dir.push("sled");
    sled_dir.push("db-0.34.6");

    let db = sled::Config::new().path(sled_dir).open()?;
    let store = Store::build(db).await?;

    let shutdown = Arc::new(Notify::new());

    let mut handles = vec![];

    #[cfg(feature = "ipc-dbus")]
    {
        dbus::Dbus::build(
            shutdown.clone(),
            store.clone(),
            input_tx.clone(),
            obs_tx.clone(),
            manager_tx.clone(),
        )
        .await?
        .run()
        .await;
    }

    handles.push(tokio::spawn(input::task(
        store.clone(),
        input_rx,
        obs_tx.clone(),
    )));
    handles.push(tokio::spawn(obs::task(store, obs_tx, obs_rx)));
    handles.push(tokio::spawn(manager::task(
        input_tx, manager_tx, manager_rx,
    )));

    tokio::select! {
        _ = shutdown.notified() => {},
        _ = tokio::signal::ctrl_c() => {},
    };

    log::info!("Application closing");

    for hdnl in &handles {
        hdnl.abort();
    }

    for hdnl in handles {
        let _ = hdnl.await;
    }

    Ok(())
}
